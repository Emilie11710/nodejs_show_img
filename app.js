const express = require ('express');
const bodyParser = require ('body-parser');
const app = express();
app.use(bodyParser.json());
const path = require('path');

const db = require ('./db');
const collection = "img";

app.get('/',(req,res)=>{
    res.sendFile(path.join(__dirname,'index.html'))
});

//getAll method
app.get('/getImgs',(req,res)=>{
    db.getDB().collection(collection).find({}).toArray((err,documents)=>{
        if(err)
            console.log(err);
        else{
            console.log(documents);
            res.json(documents);
        }
    });
});

//update method
app.put('/:id',(req, res)=>{
    const imgId = req.params.id;
    const userInput = req.body;

    db.getDB.collection(collection).findOneAndUpdate({__id : db.getPrimaryKey(imgId)},{$set : {url : userInput.url, title : userInput.title}},{returnOriginal : false},(err,result)=>{
        if(err)
            console.log(err);
        else
            res.json(result); 
    });
});

//create method
app.post('/',(req,res)=>{
    const userInput = req.body;
    db.getDB().collection(collection).insertOne(userInput, (err, result)=>{
        if(err)
            console.log(err);
        else
            res.json({result : result, document : result.ops[0]});
    });
}); 

//delete method
app.delete('/:id',(req,res)=>{
    const imgId = req.params.id;

    db.getDB().collection(collection).findOneAndDelete({__id : db.getPrimaryKey(imgId)},(err,result)=>{
        if(err)
            console.log(err);
        else
            res.json(result);
    });
});

//connection to database
db.connect((err)=>{
    if(err){
        console.log('unable to connect to database');
        process.exit(1);
    }else{
        app.listen(3000,()=>{
            console.log('connected to database, app listening on port 3000')
        })
    }
})